-- Write your upward migration in this file. You can also create an `up` directory to have separate SQL files instead.
create table if not exists my_table (
  id integer primary key autoincrement
);
