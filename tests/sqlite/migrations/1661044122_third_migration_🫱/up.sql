-- Write your upward migration in this file. You can also create an `up` directory to have separate SQL files instead.
create table if not exists table_in_third_migration (
  id integer primary key autoincrement,
  notes text null
);
