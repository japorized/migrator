# migrator

A simple database migration tool with SQL as the migration language, suitable for application development.

Currently supports only sqlite, with Postgres untested.

---

## Installation

```
git clone https://gitlab.com/japorized/migrator.git
cd migrator
cargo build --release
cp ./target/release/migrator /usr/local/bin/
```

## Usage

```
migrator --help
```

Migrator models migrations on a vertical timeline, where the earliest time is
below, and the latest is above. "Upward" migrations runs migrations that brings
the target database closer to the "newer" migrations until we get to the latest
migration plan. "Downward" migrations essentially rolls back the state of the
database to an earlier schema.
