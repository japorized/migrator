use anyhow::Result;
use shellexpand;
use std::{env, fs, io};
use thiserror::Error as TError;

#[derive(TError, Debug)]
pub enum ConfigError {
    #[error("DB_STRING is not provided")]
    NoDbString,
    #[error("DB_STRING is not unicode: {0}")]
    NotUnicode(String),
    #[error("Failed creating migration directory")]
    CannotCreateMigrationDir(io::Error),
}

#[derive(Debug)]
pub struct Config {
    pub database_string: String,
    pub migration_table: String,
    pub migration_directory: String,
}

impl Config {
    pub fn new() -> Result<Self> {
        let database_string = Self::read_db_string()?;
        let migration_table: String = shellexpand::full(
            &env::var("MIGRATOR_MIGRATION_TABLE")
                .unwrap_or_else(|_| "_migrator_migrations".to_string()),
        )
        .expect("Could not fully expand env vars. Did you use an undefined env var?")
        .to_string();
        let migration_directory = shellexpand::full(
            &env::var("MIGRATOR_MIGRATION_DIR").unwrap_or_else(|_| "./migrations".to_string()),
        )
        .expect("Could not fully expand env vars. Did you use an undefined env var?")
        .to_string();

        Self::ensure_migration_dir(&migration_directory)?;

        Ok(Self {
            database_string,
            migration_table,
            migration_directory,
        })
    }

    #[inline(always)]
    fn read_db_string() -> Result<String, ConfigError> {
        match env::var("DB_STRING") {
            Ok(s) => Ok(s),
            Err(err) => match err {
                env::VarError::NotPresent => Err(ConfigError::NoDbString),
                env::VarError::NotUnicode(s) => {
                    Err(ConfigError::NotUnicode(s.to_str().unwrap().to_string()))
                }
            },
        }
    }

    #[inline(always)]
    fn ensure_migration_dir(migration_directory: &str) -> Result<(), ConfigError> {
        match fs::create_dir_all(migration_directory) {
            Ok(_) => Ok(()),
            Err(err) => Err(ConfigError::CannotCreateMigrationDir(err)),
        }
    }
}
