use sqlx::types::chrono::NaiveDateTime;

#[derive(sqlx::FromRow, Debug, Clone)]
pub struct Migration {
    pub id: i64,
    pub batch: i64,
    pub migration_time: NaiveDateTime,
    pub migration_name: String,
}
