use clap::Parser;

/// A simple SQL-centric migrator
#[derive(Parser, Debug)]
#[clap(version="v0.1", long_about = None, name="migrator", bin_name="migrator")]
pub enum MigratorArgs {
    #[clap(about = "Run next migration")]
    Up(Up),
    #[clap(about = "Undo last migration")]
    Down(Down),
    #[clap(about = "Run to the latest migration")]
    Latest,
    #[clap(about = "Rollback the migrations by batch")]
    Rollback(Rollback),
    #[clap(about = "Check your migration status")]
    Status,
    #[clap(about = "Create a new migration file")]
    New(New),
}

#[derive(clap::Args, Debug)]
pub struct New {
    #[clap(
        help = "Name of the migration",
        value_parser,
        required = true
    )]
    pub name: String,
}

#[derive(clap::Args, Debug)]
pub struct Up {
    #[clap(
        help = "The number of yet-to-be-run migration files to run",
        short,
        long,
        value_parser,
        default_value_t = 1
    )]
    pub times: i64,
}

#[derive(clap::Args, Debug)]
pub struct Down {
    #[clap(
        help = "The number of applied migrations to run downwards",
        short,
        long,
        value_parser,
        default_value_t = 1
    )]
    pub times: i64,
}

#[derive(clap::Args, Debug)]
pub struct Rollback {
    #[clap(
        help = "Rollback all migration batches (overrides --times)",
        short,
        long,
        value_parser,
        default_value_t = false
    )]
    pub all: bool,
    #[clap(
        help = "Rollback n batches of migrations",
        short,
        long,
        value_parser,
        default_value_t = 1
    )]
    pub times: i64,
}
