use thiserror::Error as TError;

#[derive(TError, Debug)]
pub enum AppError {
    #[error("Missing either down.sql file or down directory")]
    MissingDownFiles,
    #[error("Missing either up.sql file or up directory")]
    MissingUpFiles,
}
