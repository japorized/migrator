use crate::{config::Config, models};
use anyhow::Result;
use owo_colors::*;
use sqlx::{Any, Pool};
use std::ffi::OsString;
use std::fs::{self, DirEntry};
use std::io;
use thiserror::Error as TError;

#[derive(TError, Debug)]
pub enum StatusError {
    #[error("Migration is corrupted")]
    MigrationCorrupted,
}

pub async fn get_status(config: &Config, pool: &Pool<Any>) -> Result<Vec<(OsString, bool)>> {
    let migration_dir = fs::read_dir(&config.migration_directory).unwrap();
    // Each item is a tuple, with the first element being the migration name, while the second
    // whether this particular migration is done.
    // Since we don't have data from the database at this point, we'll initialize the second
    // element as `false`.
    let mut migrations_in_fs: Vec<(OsString, bool)> = {
        let mut dir_entries: Vec<DirEntry> = migration_dir
            .into_iter()
            .map(|p| p.expect("Could not read file in directory"))
            .collect();
        dir_entries.sort_by_key(|elem| elem.path());
        dir_entries
            .iter()
            .map(|p| (p.file_name().to_os_string(), false))
            .collect()
    };

    let migrations_in_db: Vec<models::Migration> =
        sqlx::query_as::<_, models::Migration>(&format!(
            "SELECT * FROM {} ORDER BY batch, migration_time;",
            &config.migration_table,
        ))
        .fetch_all(pool)
        .await?;

    let (missing_migrations_in_fs, is_corrupted): (Vec<String>, bool) =
        migrations_in_db.into_iter().enumerate().fold(
            (vec![], false),
            |(mut missing_migrations, mut is_corrupted), (idx, migration_in_db)| {
                match migrations_in_fs.get_mut(idx) {
                    Some(migration_in_fs) => {
                        let migration_in_fs_name = &migration_in_fs.0;
                        if *migration_in_fs_name == OsString::from(&migration_in_db.migration_name)
                        {
                            migration_in_fs.1 = true;
                        } else {
                            println!(
                                "{}",
                                format!(
                                    "Expected {} but found {} at position {}",
                                    migration_in_db.migration_name,
                                    migration_in_fs_name.to_str().unwrap(),
                                    idx,
                                )
                                .red()
                            );
                            is_corrupted = true;
                        }
                    }
                    None => {
                        is_corrupted = true;
                        missing_migrations.push(migration_in_db.migration_name);
                    }
                };

                (missing_migrations, is_corrupted)
            },
        );

    if is_corrupted {
        if !missing_migrations_in_fs.is_empty() {
            println!(
                "{}",
                format!(
                    "The following migrations are not found in {}:",
                    config.migration_directory
                )
                .red()
            );
            for missing_migration in missing_migrations_in_fs {
                println!("  {}", missing_migration.red());
            }
        }

        Err(StatusError::MigrationCorrupted.into())
    } else {
        Ok(migrations_in_fs)
    }
}
