use crate::app::AppError;
use crate::config::Config;
use owo_colors::*;
use sqlx::any::AnyKind;
use sqlx::{AnyConnection, AnyExecutor};
use std::fmt::Display;
use std::fs::{self, DirEntry};
use std::io::Error;
use std::path;

/// The direction of the migration
enum MigrationDirection {
    /// Specify an upward migration.
    ///
    /// An upward migration runs the files in either the `up` directory, or the `up.sql` file, for
    /// a particular migration plan.
    Up,
    /// Specify an downward migration.
    ///
    /// An downward migration runs the files in either the `down` directory, or the `down.sql` file,
    /// for a particular migration plan.
    Down,
}

impl Display for MigrationDirection {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            MigrationDirection::Up => write!(f, "up"),
            MigrationDirection::Down => write!(f, "down"),
        }
    }
}

impl TryFrom<&str> for MigrationDirection {
    type Error = &'static str;
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value.to_lowercase().trim() {
            "up" => Ok(MigrationDirection::Up),
            "down" => Ok(MigrationDirection::Down),
            _ => Err("Unrecognized migration direction"),
        }
    }
}

/// Migrate updwards by one
pub async fn migrate_up(
    config: &Config,
    executor: &mut AnyConnection,
    migration_dir: &path::PathBuf,
    batch_num: i64,
) -> anyhow::Result<()> {
    let migration_name = migration_dir.file_name().unwrap().to_os_string();
    run_migration(&mut *executor, migration_dir, MigrationDirection::Up).await?;

    let migration_record_query = match &executor.kind() {
        AnyKind::Postgres => format!(
            "INSERT INTO {} (batch, migration_name) VALUES ($1, $2)",
            &config.migration_table
        ),
        AnyKind::MySql => unimplemented!(),
        AnyKind::Sqlite => format!(
            "INSERT INTO {} (batch, migration_name) VALUES (?, ?)",
            &config.migration_table
        ),
    };
    sqlx::query(&migration_record_query)
        .bind(batch_num)
        .bind(&migration_name.to_str().unwrap())
        .execute(&mut *executor)
        .await?;

    Ok(())
}

/// Migrate downwards by one
pub async fn migrate_down(
    config: &Config,
    executor: &mut AnyConnection,
    migration_dir: &path::PathBuf,
) -> anyhow::Result<()> {
    run_migration(&mut *executor, migration_dir, MigrationDirection::Down).await?;

    let migration_name = migration_dir.file_name().unwrap().to_os_string();
    sqlx::query(&format!(
        "DELETE FROM {} WHERE migration_name = ?",
        &config.migration_table
    ))
    .bind(&migration_name.to_str().unwrap())
    .execute(&mut *executor)
    .await?;

    Ok(())
}

/// Run the migration file, or directory of migration files, for the given direction, in the given
/// directory.
///
/// If the migration direction has a directory, use that instead of the single SQL file for that
/// direction. E.g., if the migration direction was up, and if both the `up` directory and the
/// `up.sql` file exists in the given directory, only the SQL files in the `up` directory will be
/// ran, and the `up.sql` file will be ignored.
async fn run_migration(
    executor: &mut AnyConnection,
    migration_dir: &path::PathBuf,
    direction: MigrationDirection,
) -> anyhow::Result<()> {
    let migration_name_os_string = migration_dir.file_name().unwrap().to_os_string();
    let migration_name = migration_name_os_string.to_str().unwrap();
    println!("  {} {}", "Migrating".blue(), migration_name.blue());

    let mut migration_file = path::PathBuf::from(&migration_dir);
    migration_file.push(format!("{}.sql", direction));
    let mut migration_subdir = path::PathBuf::from(&migration_dir);
    migration_subdir.push(direction.to_string());

    if migration_subdir.is_dir() {
        let mut migration_files: Vec<path::PathBuf> = fs::read_dir(&migration_subdir)
            .unwrap()
            .into_iter()
            .filter_map(filtrate_sql_files)
            .collect();
        migration_files.sort_by_key(|elem| elem.file_name().unwrap().to_os_string());
        for file in migration_files {
            read_and_execute_sql(&file, &mut *executor).await?;
        }
    } else if migration_file.is_file() {
        read_and_execute_sql(&migration_file, &mut *executor).await?;
    } else {
        println!(
            "{} {}",
            migration_name.red(),
            format!(
                "does not contain either a {}.sql file or a {}/ directory",
                direction, direction
            )
            .red()
        );
        match direction {
            MigrationDirection::Up => return Err(AppError::MissingUpFiles.into()),
            MigrationDirection::Down => return Err(AppError::MissingDownFiles.into()),
        }
    }

    Ok(())
}

/// A filtration function to get only SQL files.
fn filtrate_sql_files(f: Result<DirEntry, Error>) -> Option<path::PathBuf> {
    if f.is_err() {
        return None;
    }

    let dir_entry = f.unwrap().path();
    if !dir_entry.is_file() || dir_entry.extension().is_none() {
        return None;
    }
    if dir_entry.extension().unwrap() != "sql" {
        return None;
    }

    Some(dir_entry)
}

/// Read and execute a given SQL file.
async fn read_and_execute_sql(
    file: &path::PathBuf,
    executor: impl AnyExecutor<'_>,
) -> Result<(), sqlx::Error> {
    let contents: String = match fs::read(&file) {
        Ok(b) => String::from_utf8_lossy(&b).parse().unwrap(),
        Err(err) => {
            return Err(err.into());
        }
    };
    sqlx::query(&contents).execute(executor).await?;

    Ok(())
}
