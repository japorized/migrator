pub mod app;
mod cli;
mod config;
mod models;
/// Various migration-running instructions
mod runner;
mod status;

use anyhow::Result;
use clap::Parser;
pub use cli::MigratorArgs;
pub use config::Config;
use owo_colors::*;
use sqlx;
use sqlx::any::{AnyKind, AnyPoolOptions};
use sqlx::types::chrono::Utc;
use sqlx::{AnyExecutor, Row};
use std::io::Write;
use std::{fs, path};

use self::cli::{New, Rollback};
use self::status::get_status;

#[tokio::main]
async fn main() -> Result<()> {
    let args = MigratorArgs::parse();
    let config = Config::new()?;

    run(&args, &config).await
}

/// Run migrator.
///
/// This is the entrypoint for the entire program
pub async fn run(args: &MigratorArgs, config: &Config) -> Result<()> {
    let pool = AnyPoolOptions::new()
        .connect(&config.database_string)
        .await?;

    ensure_migration_table(&config, &pool).await?;

    let handling_result = match args {
        MigratorArgs::Up(opts) => handle_up(&config, Some(opts.times), &pool).await,
        MigratorArgs::Down(opts) => handle_down(&config, Some(opts.times), &pool).await,
        MigratorArgs::New(opts) => handle_new(&config, &opts),
        MigratorArgs::Latest => handle_up(&config, None, &pool).await,
        MigratorArgs::Rollback(opts) => handle_rollback(&config, opts, &pool).await,
        MigratorArgs::Status => show_status(&config, &pool).await,
    };
    if let Err(err) = handling_result {
        println!("{}", err.red());
        std::process::exit(1);
    }

    Ok(())
}

async fn handle_up(
    config: &Config,
    max_times: Option<i64>,
    pool: &sqlx::Pool<sqlx::Any>,
) -> Result<()> {
    println!("{}", "Starting upward migration".blue());
    let migrations_in_fs: Vec<path::PathBuf> = get_status(config, pool)
        .await?
        .into_iter()
        .filter_map(|(migration_name, is_migrated)| {
            if is_migrated {
                None
            } else {
                let mut p = path::PathBuf::from(&config.migration_directory);
                p.push(migration_name);
                Some(p)
            }
        })
        .collect();
    let mut counter = 0;
    let mut trx = pool.begin().await?;
    let batch_num = get_next_batch_num(config, &mut trx).await?;
    for migration in migrations_in_fs {
        runner::migrate_up(config, &mut trx, &migration, batch_num).await?;

        counter += 1;
        if let Some(max) = max_times {
            if counter == max {
                break;
            }
        }
    }
    trx.commit().await?;
    println!("{}", "Migration complete!".green());
    println!("Ran {} migration(s)", counter.green());

    Ok(())
}

async fn handle_down(
    config: &Config,
    max_times: Option<i64>,
    pool: &sqlx::Pool<sqlx::Any>,
) -> Result<()> {
    println!("{}", "Starting downward migration".blue());
    let mut completed_migrations: Vec<path::PathBuf> = get_status(config, pool)
        .await?
        .into_iter()
        .filter_map(|(migration_name, is_migrated)| {
            if is_migrated {
                let mut p = path::PathBuf::from(&config.migration_directory);
                p.push(migration_name);
                Some(p)
            } else {
                None
            }
        })
        .collect();
    completed_migrations.reverse();
    let mut counter = 0;
    let mut trx = pool.begin().await?;
    for migration in completed_migrations {
        runner::migrate_down(config, &mut trx, &migration).await?;

        counter += 1;
        if let Some(max) = max_times {
            if counter == max {
                break;
            }
        }
    }
    trx.commit().await?;
    println!("{}", "Migration complete!".green());
    println!("Undid {} migration(s)", counter.green());

    Ok(())
}

async fn handle_rollback(
    config: &Config,
    opts: &Rollback,
    pool: &sqlx::Pool<sqlx::Any>,
) -> anyhow::Result<()> {
    if opts.times < 1 {
        println!("{}", "option --times must be greater than 0".red());
        std::process::exit(1);
    }
    println!("{}", "Starting rollback(s)".blue());
    // Verify that the migration isn't corrupted.
    // There's no need to use the returned values.
    get_status(config, pool).await?;

    let mut counter = 0;
    let mut trx = pool.begin().await?;

    let db_migrations: Vec<models::Migration> = if opts.all {
        sqlx::query_as::<_, models::Migration>(&format!(
            "SELECT * FROM {} ORDER BY id DESC;",
            &config.migration_table
        ))
        .fetch_all(&mut trx)
        .await?
    } else {
        // Get distinct batches based on the times parameter
        sqlx::query_as::<_, models::Migration>(&format!(
            r"
            SELECT * FROM {}
            WHERE batch IN (
                SELECT DISTINCT batch FROM {}
                ORDER BY batch DESC LIMIT ?
            )
            ORDER BY id DESC;",
            &config.migration_table, &config.migration_table,
        ))
        .bind(opts.times)
        .fetch_all(&mut trx)
        .await?
    };

    if db_migrations.is_empty() {
        println!("{}", "Already at start of migrations!".green());
        return Ok(());
    }

    for migration in &db_migrations {
        let mut migration_dir = path::PathBuf::from(&config.migration_directory);
        migration_dir.push(&migration.migration_name);
        runner::migrate_down(config, &mut trx, &migration_dir).await?;

        counter += 1;
    }

    trx.commit().await?;
    println!("{}", "Migration complete!".green());
    println!("Undid {} migration(s)", counter.green());
    println!(
        "{} {} {}",
        "Rolled back".green(),
        db_migrations
            .into_iter()
            .fold(vec![], |mut batch_nums, m| {
                if !batch_nums.contains(&m.batch) {
                    batch_nums.push(m.batch);
                }

                batch_nums
            })
            .len()
            .green(),
        "batch(es)".green(),
    );

    Ok(())
}

fn handle_new(config: &Config, cmd_flags: &New) -> Result<()> {
    let current_unix_epoch = Utc::now().timestamp();
    let migration_name = format!("{}_{}", current_unix_epoch, cmd_flags.name);
    let mut new_migration_dir_path = path::PathBuf::from(&config.migration_directory);
    new_migration_dir_path.push(&migration_name);
    let mut up_file_path = path::PathBuf::from(&new_migration_dir_path);
    up_file_path.push("up.sql");
    let mut down_file_path = path::PathBuf::from(&new_migration_dir_path);
    down_file_path.push("down.sql");

    fs::create_dir_all(new_migration_dir_path)?;
    let mut up_file = fs::File::create(up_file_path)?;
    up_file.write_all(b"-- Write your upward migration in this file. You can also create an `up` directory to have separate SQL files instead.")?;
    let mut down_file = fs::File::create(down_file_path)?;
    down_file.write_all(b"-- Write your downward migration in this file. You can also create a `down` directory to have separate SQL files instead.")?;
    println!(
        "{} {}",
        "Successfully created migration:".green(),
        migration_name,
    );

    Ok(())
}

async fn show_status(config: &Config, pool: &sqlx::Pool<sqlx::Any>) -> Result<()> {
    let migrations_in_fs = get_status(config, pool).await?;

    let (migrations_ran, migrations_not_ran): (Vec<String>, Vec<String>) =
        migrations_in_fs.into_iter().fold(
            (vec![], vec![]),
            |(mut migrations_ran, mut migrations_not_ran), migration| {
                if migration.1 {
                    migrations_ran.push(migration.0.to_str().unwrap().to_string());
                } else {
                    migrations_not_ran.push(migration.0.to_str().unwrap().to_string());
                }

                (migrations_ran, migrations_not_ran)
            },
        );
    if !migrations_ran.is_empty() {
        println!("{}", "Completed migrations:".green());
        for migration in &migrations_ran {
            println!("  {}", migration.green());
        }
    }

    if !migrations_not_ran.is_empty() {
        println!("{}", "Pending migrations:".yellow());
        for migration in &migrations_not_ran {
            println!("  {}", migration.yellow());
        }
    }

    if migrations_ran.is_empty() && migrations_not_ran.is_empty() {
        println!("{}", "No migrations yet!".yellow());
    }

    Ok(())
}

async fn ensure_migration_table(config: &Config, pool: &sqlx::Pool<sqlx::Any>) -> Result<()> {
    match &pool.any_kind() {
        AnyKind::Postgres => {
            let q = format!(
                r"CREATE TABLE IF NOT EXISTS {} (
                    id SERIAL PRIMARY KEY NOT NULL,
                    batch INT8 NOT NULL,
                    migration_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    migration_name VARCHAR NOT NULL
                );",
                &config.migration_table,
            );
            sqlx::query(&q).execute(pool).await?;
        }
        AnyKind::MySql => todo!(),
        AnyKind::Sqlite => {
            let q = format!(
                r"CREATE TABLE IF NOT EXISTS {} (
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    batch INT NOT NULL,
                    migration_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    migration_name VARCHAR NOT NULL
                );",
                &config.migration_table,
            );
            sqlx::query(&q).execute(pool).await?;
        }
    }

    Ok(())
}

async fn get_next_batch_num<'a>(config: &Config, executor: impl AnyExecutor<'_>) -> Result<i64> {
    let row = sqlx::query(&format!(
        "SELECT COALESCE(MAX(batch), 0) + 1 FROM {}",
        &config.migration_table
    ))
    .fetch_one(executor)
    .await?;
    let batch = row.get(0);

    Ok(batch)
}

#[cfg(test)]
mod test {
    use std::fs::DirEntry;
    use std::{fs, io, path};

    use sqlx::any::AnyPoolOptions;
    use sqlx::migrate::MigrateDatabase;
    use sqlx::AnyExecutor;

    use super::{ensure_migration_table, handle_down, handle_new, handle_rollback, handle_up};
    use crate::cli;
    use crate::config::Config;

    fn setup_sqlite3(db_name: Option<&str>) -> Config {
        Config {
            database_string: {
                let s = "/tmp/migrator_tests/sqlite";
                fs::create_dir_all(&s).expect("Could not create test dir");
                match db_name {
                    Some(n) => {
                        fs::create_dir_all(format!("{}/{}", s, n))
                            .expect("Could not create test dir");
                        format!("sqlite:///tmp/migrator_tests/sqlite/{}/db", n)
                    }
                    None => "sqlite:///tmp/migrator_tests/sqlite/dev.db".to_owned(),
                }
            },
            migration_table: "_migrator_migrations".to_owned(),
            migration_directory: "./tests/sqlite/migrations".to_owned(),
        }
    }

    async fn does_sqlite_table_exist(
        table_name: &str,
        executor: impl AnyExecutor<'_>,
    ) -> anyhow::Result<bool> {
        let (does_table_exist,): (bool,) = sqlx::query_as(
            r"SELECT EXISTS (
                SELECT 1
                FROM sqlite_master
                WHERE
                    type = 'table'
                    AND name = ?
                LIMIT 1
            )",
        )
        .bind(table_name)
        .fetch_one(executor)
        .await?;

        Ok(does_table_exist)
    }

    #[tokio::test]
    async fn test_handle_new() -> anyhow::Result<()> {
        let config = Config {
            database_string: "sqlite:///tmp/migrator_tests/sqlite/test_handle_new/db".to_owned(),
            migration_table: "_migrator_migrations".to_owned(),
            migration_directory: "/tmp/migrator_tests/sqlite/test_handle_new/migrations".to_owned(),
        };
        handle_new(
            &config,
            &cli::New {
                name: "migrate_for_foo".to_owned(),
            },
        )?;

        let new_migration_dir = path::PathBuf::from(&config.migration_directory);
        let is_created_file_in_migration_dir: bool = fs::read_dir(&new_migration_dir)
            .expect("Could not read migration dir")
            .into_iter()
            .filter(|maybe_dir| match maybe_dir {
                Ok(dir) => {
                    if dir
                        .path()
                        .file_name()
                        .unwrap()
                        .to_string_lossy()
                        .to_string()
                        .contains("migrate_for_foo")
                    {
                        true
                    } else {
                        false
                    }
                }
                Err(_) => false,
            })
            .collect::<Vec<Result<DirEntry, io::Error>>>()
            .len()
            == 1;
        assert!(is_created_file_in_migration_dir);
        fs::remove_dir_all("/tmp/migrator_tests/sqlite/test_handle_new/")?;

        Ok(())
    }

    #[tokio::test]
    async fn test_sqlite3_up_and_down() -> anyhow::Result<()> {
        let config = setup_sqlite3(Some("migration_up_and_down"));
        sqlx::sqlite::Sqlite::create_database(&config.database_string).await?;
        let pool = AnyPoolOptions::new()
            .connect(&config.database_string)
            .await?;
        ensure_migration_table(&config, &pool).await?;

        assert!(!does_sqlite_table_exist("my_table", &pool).await?);
        handle_up(&config, Some(1), &pool).await?;
        assert!(does_sqlite_table_exist("my_table", &pool).await?);
        handle_down(&config, Some(1), &pool).await?;
        assert!(!does_sqlite_table_exist("my_table", &pool).await?);

        handle_up(&config, Some(3), &pool).await?;
        assert!(does_sqlite_table_exist("table_in_third_migration", &pool).await?);
        assert!(does_sqlite_table_exist("table_3", &pool).await?);
        handle_down(&config, Some(1), &pool).await?;
        assert!(!does_sqlite_table_exist("table_in_third_migration", &pool).await?);
        assert!(does_sqlite_table_exist("table_3", &pool).await?);
        handle_down(&config, Some(5), &pool).await?;

        fs::remove_dir_all("/tmp/migrator_tests/sqlite/migration_up_and_down/")?;

        Ok(())
    }

    #[tokio::test]
    async fn test_sqlite3_latest_and_rollback() -> anyhow::Result<()> {
        let config = setup_sqlite3(Some("migration_latest_and_rollback"));
        sqlx::sqlite::Sqlite::create_database(&config.database_string).await?;
        let pool = AnyPoolOptions::new()
            .connect(&config.database_string)
            .await?;
        ensure_migration_table(&config, &pool).await?;

        handle_up(&config, None, &pool).await?;
        handle_rollback(
            &config,
            &cli::Rollback {
                all: true,
                times: 1,
            },
            &pool,
        )
        .await?;

        fs::remove_dir_all("/tmp/migrator_tests/sqlite/migration_latest_and_rollback/")?;

        Ok(())
    }

    #[tokio::test]
    async fn test_sqlite3_up_and_rollback() -> anyhow::Result<()> {
        let config = setup_sqlite3(Some("migration_up_and_rollback"));
        sqlx::sqlite::Sqlite::create_database(&config.database_string).await?;
        let pool = AnyPoolOptions::new()
            .connect(&config.database_string)
            .await?;
        ensure_migration_table(&config, &pool).await?;

        handle_up(&config, Some(1), &pool).await?;
        assert!(!does_sqlite_table_exist("table_in_third_migration", &pool).await?);
        handle_up(&config, Some(2), &pool).await?;
        assert!(does_sqlite_table_exist("table_in_third_migration", &pool).await?);
        handle_rollback(
            &config,
            &cli::Rollback {
                all: false,
                times: 1,
            },
            &pool,
        )
        .await?;
        assert!(!does_sqlite_table_exist("table_in_third_migration", &pool).await?);
        handle_rollback(
            &config,
            &cli::Rollback {
                all: false,
                times: 1,
            },
            &pool,
        )
        .await?;
        assert!(!does_sqlite_table_exist("my_table", &pool).await?);

        handle_up(&config, Some(1), &pool).await?;
        handle_up(&config, Some(2), &pool).await?;
        handle_rollback(
            &config,
            &cli::Rollback {
                all: false,
                times: 2,
            },
            &pool,
        )
        .await?;
        assert!(!does_sqlite_table_exist("my_table", &pool).await?);

        fs::remove_dir_all("/tmp/migrator_tests/sqlite/migration_up_and_rollback/")?;

        Ok(())
    }
}
